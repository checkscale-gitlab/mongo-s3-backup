#!/bin/sh
set -e

echo -e "section_start:$( date +%s ):trb_setup\r\033[0KSetup code for tag, release and build."

# Add curl, jq git
apk add curl jq git # git is not installed by the runner itself?

git checkout master
git pull

# fetch latest version
CURRENT_VERSION=$(git tag --sort=committerdate | tail -1)
NUM_COMMITS_SINCE_LAST_VERSION=$(git rev-list HEAD...$CURRENT_VERSION --count)

echo -e "section_end:$( date +%s ):trb_setup\r\033[0K"

if [ $NUM_COMMITS_SINCE_LAST_VERSION -lt 1 ]
then
    echo
    echo "Last version commit ref: $CURRENT_VERSION"
    echo
    echo "Last commit is a tag, skip creating a new tag."
    exit 1;
fi

NEXT_VERSION=${CURRENT_VERSION%.*}.$((${CURRENT_VERSION##*.}+1))

echo
echo "About to auto tag, release and build image."
echo "  Current version: $CURRENT_VERSION"
echo "  Commits since last version: $NUM_COMMITS_SINCE_LAST_VERSION"
echo "  New version: $NEXT_VERSION"
echo

echo -e "section_start:$( date +%s ):trb_tag\r\033[0KCreate Tag $NEXT_VERSION"

# Create git tag via API
curl --request POST --header "PRIVATE-TOKEN: $TAG_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/repository/tags?tag_name=$NEXT_VERSION&ref=master"

echo "Tag $NEXT_VERSION created (via API)"

echo -e "section_end:$( date +%s ):trb_tag\r\033[0K"

echo -e "section_start:$( date +%s ):trb_release\r\033[0KCreate release for $NEXT_VERSION"

echo "About to build release"

# Gather info to build a release
COMMIT_MSGS=$(git log --pretty="format:%h %s" HEAD...$CURRENT_VERSION);
# Format commit messages as markdown bullet list.
COMMIT_MSGS_MD=$(echo "$COMMIT_MSGS" | awk '{print " * ", $0}')

# Fetch mongodb version from Dockerfile
MONGO_VERSION=$(head -n 1 Dockerfile | awk -F ':' '{print $2}')

echo "  Current MongoDB version: $MONGO_VERSION"

RELEASE_DESC="Docker image: \`$CI_REGISTRY_IMAGE:$NEXT_VERSION\`

|Component|version|
|-------|-------|
|MongoDB| $MONGO_VERSION |


Changes:

$COMMIT_MSGS_MD

_Release created by GitLab CI pipeline._ :robot:"

JSON=$(jq -nc --arg n "Mongo S3 backup $NEXT_VERSION" --arg t "$NEXT_VERSION" --arg d "$RELEASE_DESC" '{"name": $n, "tag_name": $t, "description": $d}')

curl --header 'Content-Type: application/json' --header "PRIVATE-TOKEN: $TAG_TOKEN" --data "$JSON" --request POST https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/releases

echo
echo "Done publishing release."
echo

echo -e "section_end:$( date +%s ):trb_release\r\033[0K"

echo -e "section_start:$( date +%s ):trb_build\r\033[0KBuild docker images for $NEXT_VERSION"

# building image
echo
echo "Building image"
docker build --pull -t "$CI_REGISTRY_IMAGE:$NEXT_VERSION" -t "$CI_REGISTRY_IMAGE:latest" .
docker push "$CI_REGISTRY_IMAGE:$NEXT_VERSION"
docker push "$CI_REGISTRY_IMAGE:latest"

echo "Done building image."

echo -e "section_end:$( date +%s ):trb_build\r\033[0K"

echo
echo "Done tagging, releasing and building image."
